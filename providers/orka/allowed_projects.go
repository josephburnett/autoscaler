package orka

import (
	"bufio"
	"os"
	"sort"
	"strings"
)

type allowedProjects []string

func (list allowedProjects) Contains(project string) bool {
	project = strings.ToLower(project)
	project = ensureFullPathComponent(project)
	idx := sort.SearchStrings(list, project)

	// identical match
	if idx < len(list) && list[idx] == project {
		return true
	}

	if idx-1 < 0 {
		return false
	}

	return strings.HasPrefix(project, list[idx-1])
}

func loadAllowedProjects(pathname string) (allowedProjects, error) {
	f, err := os.Open(pathname)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var allowed allowedProjects

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		prefix := strings.TrimSpace(strings.SplitN(scanner.Text(), "#", 2)[0])
		if prefix == "" {
			continue
		}

		prefix = strings.ToLower(prefix)
		allowed = append(allowed, ensureFullPathComponent(prefix))
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	sort.Strings(allowed)

	return allowed, nil
}

// ensureFullPathComponent adds a terminating '/' to any path given, to ensure
// that we're only ever matching a full path component, rather than a partial
// path (`/group/subgroup/` rather than `/group/subgr`)
func ensureFullPathComponent(pathname string) string {
	if !strings.HasSuffix(pathname, "/") {
		return pathname + "/"
	}
	return pathname
}
