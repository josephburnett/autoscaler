package dial

import (
	"context"
	"net"
	"testing"
	"time"

	gliderSSH "github.com/gliderlabs/ssh"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/crypto/ssh"
)

func TestSSHWithDeadline(t *testing.T) {
	t.Run("normal case", func(t *testing.T) {
		addr, cleanup := dummyListener(t, func(*gliderSSH.Server) error { return nil })
		defer cleanup()

		_, err := SSHWithDeadline(context.Background(), addr.Network(), addr.String(), &ssh.ClientConfig{
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
			Timeout:         DefaultSSHTimeout,
		})
		require.NoError(t, err)
	})

	t.Run("connection deadline exceeded", func(t *testing.T) {
		addr, cleanup := dummyListener(t, gliderSSH.WrapConn(func(ctx gliderSSH.Context, conn net.Conn) net.Conn {
			time.Sleep(200 * time.Millisecond)
			return conn
		}))
		defer cleanup()

		_, err := SSHWithDeadline(context.Background(), addr.Network(), addr.String(), &ssh.ClientConfig{
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
			Timeout:         100 * time.Millisecond,
		})
		require.Error(t, err)
		assert.Contains(t, err.Error(), "ssh handshake failed: context deadline exceeded")
	})

	t.Run("dial fail", func(t *testing.T) {
		_, err := SSHWithDeadline(context.Background(), "tcp", "127.0.0.1", &ssh.ClientConfig{
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		})
		require.Error(t, err)
		assert.Contains(t, err.Error(), "dialing with timeout: dial tcp:")
	})

	t.Run("parent context canceled during handshake", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())

		addr, cleanup := dummyListener(t, gliderSSH.WrapConn(func(ctx gliderSSH.Context, conn net.Conn) net.Conn {
			cancel()
			return conn
		}))
		defer cleanup()

		_, err := SSHWithDeadline(ctx, addr.Network(), addr.String(), &ssh.ClientConfig{
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
			Timeout:         DefaultSSHTimeout,
		})
		require.Error(t, err)
		assert.Contains(t, err.Error(), "ssh handshake failed: context canceled")
	})
}

func dummyListener(t *testing.T, option gliderSSH.Option) (net.Addr, func()) {
	l, err := net.Listen("tcp", "127.0.0.1:")
	require.NoError(t, err)

	srv := &gliderSSH.Server{}

	err = srv.SetOption(option)
	require.NoError(t, err)

	go func() {
		err = srv.Serve(l)
		assert.ErrorIs(t, err, gliderSSH.ErrServerClosed)
	}()

	cleanup := func() {
		err := srv.Close()
		require.NoError(t, err)
	}

	return l.Addr(), cleanup
}
