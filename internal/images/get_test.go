package images

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func resetEnv() func() {
	// ensure CUSTOM_ENV_CI_JOB_IMAGE is empty at the beginning and reset it after the test
	actual := os.Getenv("CUSTOM_ENV_CI_JOB_IMAGE")
	os.Unsetenv("CUSTOM_ENV_CI_JOB_IMAGE")
	return func() { os.Setenv("CUSTOM_ENV_CI_JOB_IMAGE", actual) }
}

func TestGet(t *testing.T) {
	fn := resetEnv()
	defer fn()

	t.Run("returns fallback when no env var is set", func(t *testing.T) {
		img, err := Get("foo-bar", []string{"foo-*"})
		require.NoError(t, err)
		assert.Equal(t, "foo-bar", img)
	})

	t.Run("also returns fallback when it is empty", func(t *testing.T) {
		img, err := Get("", []string{"foo-*"})
		require.NoError(t, err)
		assert.Equal(t, "", img)
	})

	t.Run("allow list does not apply to fallback", func(t *testing.T) {
		img, err := Get("foo-bar", []string{"bar-*"})
		require.NoError(t, err)
		assert.Equal(t, "foo-bar", img)
	})
}

func TestGetEnv(t *testing.T) {
	fn := resetEnv()
	defer fn()

	os.Setenv("CUSTOM_ENV_CI_JOB_IMAGE", "baz")

	t.Run("returns env var if set & allowed", func(t *testing.T) {
		img, err := Get("foo-bar", []string{"baz"})
		require.NoError(t, err)
		assert.Equal(t, "baz", img)
	})

	t.Run("validates env var with allowlist", func(t *testing.T) {
		_, err := Get("foo-bar", []string{"bar-*"})
		require.Error(t, err)
		assert.Equal(t, "job image \"baz\" does not match allowed image patterns. Check the AllowedImages property in your config.", err.Error())
	})

	t.Run("does not allow any image if empty string", func(t *testing.T) {
		_, err := Get("foo-bar", []string{""})
		require.Error(t, err)
		assert.Equal(t, "job image \"baz\" does not match allowed image patterns. Check the AllowedImages property in your config.", err.Error())
	})

	t.Run("returns image if env var is set and allowlist is empty", func(t *testing.T) {
		img, err := Get("foo-bar", []string{})
		require.NoError(t, err)
		assert.Equal(t, "baz", img)
	})

	t.Run("returns doublestar error", func(t *testing.T) {
		_, err := Get("foo-bar", []string{"{"})
		require.Error(t, err)
		assert.Equal(t, "matching allowed image: syntax error in pattern", err.Error())
	})
}
