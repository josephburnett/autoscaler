package backoff

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging/test"
)

func TestLoop(t *testing.T) {
	tests := map[string]struct {
		shouldBreak  bool
		err          error
		expectedErr  error
		assertOutput func(t *testing.T, output string)
	}{
		"handler exits with shouldBreak=true and no error": {
			shouldBreak: true,
			err:         nil,
			expectedErr: nil,
			assertOutput: func(t *testing.T, output string) {
				assert.Contains(t, output, `msg="Executing handler..."`)
				assert.Contains(t, output, `retry-count=0`)
				assert.NotContains(t, output, `retry-count=1`)
				assert.NotContains(t, output, `error="`)
			},
		},
		"handler exits with shouldBreak=false and no error": {
			shouldBreak: false,
			err:         nil,
			expectedErr: ErrMaximumBackOffTimeElapsedExceeded,
			assertOutput: func(t *testing.T, output string) {
				assert.Contains(t, output, `msg="Executing handler..."`)
				assert.Contains(t, output, `retry-count=0`)
				assert.Contains(t, output, `retry-count=1`)
				assert.NotContains(t, output, `error="`)
			},
		},
		"handler exits with shouldBreak=true and an error": {
			shouldBreak: true,
			err:         errors.New("test error"),
			expectedErr: nil,
			assertOutput: func(t *testing.T, output string) {
				assert.Contains(t, output, `msg="Error while handling backOff loop"`)
				assert.Contains(t, output, `error="`)
				assert.Contains(t, output, `msg="Executing handler..."`)
				assert.Contains(t, output, `retry-count=0`)
				assert.NotContains(t, output, `retry-count=1`)
			},
		},
		"handler exits with shouldBreak=false and an error": {
			shouldBreak: false,
			err:         errors.New("test error"),
			expectedErr: ErrMaximumBackOffTimeElapsedExceeded,
			assertOutput: func(t *testing.T, output string) {
				assert.Contains(t, output, `msg="Error while handling backOff loop"`)
				assert.Contains(t, output, `error="`)
				assert.Contains(t, output, `msg="Executing handler..."`)
				assert.Contains(t, output, `retry-count=0`)
				assert.Contains(t, output, `retry-count=1`)
			},
		},
		"maximum elapsed time exceeded": {
			shouldBreak: false,
			err:         nil,
			expectedErr: ErrMaximumBackOffTimeElapsedExceeded,
			assertOutput: func(t *testing.T, output string) {
				assert.Contains(t, output, `msg="Executing handler..."`)
				assert.Contains(t, output, `retry-count=0`)
				assert.Contains(t, output, `retry-count=1`)
				assert.Contains(t, output, `retry-count=2`)
			},
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			logger, output := test.NewBufferedLogger()

			err := logger.SetLevel("debug")
			require.NoError(t, err)

			settings := Settings{
				InitialInterval:     1,
				RandomizationFactor: 0,
				Multiplier:          1,
				MaxInterval:         1,
				MaxElapsedTime:      2,
			}

			err = Loop(settings, logger, func(log logging.Logger) (bool, error) {
				log.Info("Executing handler...")

				return testCase.shouldBreak, testCase.err
			})

			assert.Equal(t, testCase.expectedErr, err)
			testCase.assertOutput(t, output.String())
		})
	}
}
