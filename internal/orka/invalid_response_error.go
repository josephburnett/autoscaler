package orka

import (
	"errors"
	"fmt"
)

type InvalidResponseError struct {
	msg     string
	wrapped error
}

func NewInvalidResponseError(msg string, err error) *InvalidResponseError {
	return &InvalidResponseError{
		msg:     msg,
		wrapped: err,
	}
}

func (i *InvalidResponseError) Unwrap() error {
	return i.wrapped
}

func (i *InvalidResponseError) Error() string {
	if i.wrapped == nil {
		return i.msg
	}

	return fmt.Sprintf("%s: %s", i.msg, i.wrapped.Error())
}

func (i *InvalidResponseError) Is(err error) bool {
	var other *InvalidResponseError
	if !errors.As(err, &other) {
		return false
	}

	return other.Error() == i.Error()
}
