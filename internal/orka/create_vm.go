package orka

import (
	"context"
	"errors"
	"net/http"
	"regexp"
)

type createVMReqBody struct {
	VMName    string `json:"orka_vm_name"`
	BaseImage string `json:"orka_base_image"`
	Image     string `json:"orka_image"`
	Cores     int    `json:"orka_cpu_core"`
	VCores    int    `json:"vcpu_count"`
}

// ErrInvalidVMName see https://orkadocs.macstadium.com/docs/ui-quick-start under "VM name limitations"
var ErrInvalidVMName = errors.New("VM name must consist of lower case alphanumeric characters or '-', start with an alphabetic character, end with an alphanumeric character and may not be greater than 38 characters")
var vmNameFormat = regexp.MustCompile(`^[a-z][a-z0-9\-]{0,36}[a-z0-9]$`)

func (c *client) CreateVM(ctx context.Context, name string, baseImage string, cores int) error {
	if !vmNameFormat.MatchString(name) {
		return ErrInvalidVMName
	}

	endpoint := getVMEndpointRelURL(endpointCreate)
	reqBody := createVMReqBody{
		VMName:    name,
		BaseImage: baseImage,
		Image:     name,
		Cores:     cores,
		VCores:    cores,
	}
	return c.sendHTTPRequestWithRetries(ctx, http.MethodPost, endpoint, reqBody, nil)
}
