package orka

import (
	"errors"
	"fmt"
	"strings"
)

// HTTPError is an error returned by the Orka client when an HTTP error occurs
// The Message and Errors fields consists of values returned in the Orka API response
type HTTPError struct {
	Endpoint   string
	StatusCode int
	Message    string
	Errors     []string
}

func (e *HTTPError) Error() string {
	var fields []string
	fields = append(fields, fmt.Sprintf("%q", e.Endpoint), fmt.Sprintf("status: %d", e.StatusCode))
	if e.Message != "" {
		fields = append(fields, fmt.Sprintf("message: %s", e.Message))
	}
	if len(e.Errors) > 0 {
		fields = append(fields, fmt.Sprintf("errors: %s", strings.Join(e.Errors, "; ")))
	}

	return fmt.Sprintf("response from %s", strings.Join(fields, ", "))
}

func (e *HTTPError) Is(err error) bool {
	var httpErr *HTTPError
	ok := errors.As(err, &httpErr)
	if ok {
		ok = httpErr.StatusCode == e.StatusCode &&
			httpErr.Endpoint == e.Endpoint &&
			httpErr.Message == e.Message &&
			isStringArrayEqual(httpErr.Errors, e.Errors)
	}
	return ok
}

func isStringArrayEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}

	for i, s := range a {
		if s != b[i] {
			return false
		}
	}

	return true
}
