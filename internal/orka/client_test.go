package orka

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/backoff"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
)

const (
	testVMName = "some-vm"
	testVMIP   = "10.221.188.3"
)

var (
	// Max 1 second wait, with immediate retries, for testing purposes
	testBackoff = &backoff.Settings{
		InitialInterval:     0,
		RandomizationFactor: 0,
		Multiplier:          1,
		MaxInterval:         1,
		MaxElapsedTime:      1,
	}
)

type testCaseBase struct {
	token                   string
	statusCode              int
	respBody                interface{}
	backoffSettings         *backoff.Settings
	expectedRequestAttempts int
	expectedRequestEndpoint string
	expectedRequestBody     interface{}
	expectedErr             error
}

type testFunc func(*testing.T, *client) error
type testFuncWithContext func(context.Context, *testing.T, *client) error

func TestGetVMEndpointRelURL(t *testing.T) {
	assert.Equal(t, getVMEndpointRelURL("deploy"), "resources/vm/deploy")
	assert.Equal(t, getVMEndpointRelURL(endpointCreate, "my_vm"), "resources/vm/create/my_vm")
}

func runTestCase(t *testing.T, method string, tc *testCaseBase, testFn func(*client) error) {
	// Arrange
	hFail := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/text; charset=utf-8")

		// Write HTTP response status code
		w.WriteHeader(http.StatusServiceUnavailable)

		var b []byte
		written, err := w.Write(b)
		require.NoError(t, err)
		assert.Equal(t, len(b), written, fmt.Sprintf("written %d bytes instead of expected %d", written, len(b)))
	})
	hSuccess := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Assert HTTP request properties
		assert.Equal(t, method, r.Method, "HTTP method mismatch")
		assert.Equal(t, tc.expectedRequestEndpoint, strings.TrimPrefix(r.URL.Path, "/"), "HTTP endpoint mismatch")
		assert.Equal(t, fmt.Sprintf("Bearer %s", tc.token), r.Header.Get("Authorization"),
			"Authorization header mismatch")
		assert.Equal(t, "application/json; charset=utf-8", r.Header.Get("Content-Type"), "Content-Type header mismatch")

		// Assert HTTP request body
		reqBody, err := ioutil.ReadAll(r.Body)
		require.NoError(t, err)
		if tc.expectedRequestBody != nil {
			expectedReqBodyBytes, err := json.Marshal(tc.expectedRequestBody)
			require.NoError(t, err)
			assert.Equal(t, string(expectedReqBodyBytes), string(reqBody), "HTTP request body mismatch")
		} else {
			assert.Empty(t, string(reqBody), "HTTP request body mismatch")
		}

		// Write HTTP response body
		b, ok := tc.respBody.([]byte)
		if !ok {
			// Only marshal respBody if it's not already a byte array
			// Some tests want to provide an (invalid) byte array
			b, err = json.Marshal(tc.respBody)

			// We know that we're sending JSON, so we can add that to the headers
			// For other cases ([]byte) we just let http.DetectContentType() handle things
			w.Header().Add("Content-Type", "application/json; charset=utf-8")
		}
		require.NoError(t, err, "failure marshaling test fixture struct to JSON")

		// Write HTTP response status code
		w.WriteHeader(tc.statusCode)

		written, err := w.Write(b)
		require.NoError(t, err)
		assert.Equal(t, len(b), written, fmt.Sprintf("written %d bytes instead of expected %d", written, len(b)))
	})

	requests := 0
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requests++

		// Proxy to the intended case handler
		if requests >= tc.expectedRequestAttempts {
			hSuccess(w, r)
		} else {
			hFail(w, r)
		}
	})
	ts := httptest.NewServer(h)
	defer ts.Close()

	baseURL, err := url.Parse(ts.URL)
	require.NoError(t, err)

	config := &Config{
		Logger:          logging.New(),
		BackoffSettings: tc.backoffSettings,
		Client:          ts.Client(),
	}
	c := New(baseURL, tc.token, config)

	// Act
	err = testFn(c.(*client))

	assert.Equal(t, tc.expectedRequestAttempts, requests)
	if tc.expectedErr == nil {
		require.NoError(t, err)
		return
	}

	assert.ErrorIs(t, err, tc.expectedErr)
}

func TestHTMLResponseHandling(t *testing.T) {
	const htmlErrBody = "<HTML>error</HTML>"
	type htmlRespHandlingTestCase struct {
		testCaseBase

		method string
		testFn testFunc
	}
	testCases := map[string]htmlRespHandlingTestCase{
		"with status html response": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusProxyAuthRequired,
				respBody:                []byte(htmlErrBody),
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, testVMName),
				expectedRequestBody:     nil,
				expectedErr: &HTTPError{
					Endpoint:   getVMEndpointRelURL(endpointStatus, testVMName),
					StatusCode: http.StatusProxyAuthRequired,
					Message:    htmlErrBody,
				},
			},
			method: http.MethodGet,
			testFn: func(t *testing.T, c *client) error {
				addr, err := c.GetSSHEndpoint(context.Background(), testVMName)
				assert.Equal(t, nil, addr, "expected empty address")
				return err
			},
		},
		"with create html response": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusForbidden,
				respBody:                []byte(htmlErrBody),
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointCreate),
				expectedRequestBody:     getExpectedCreateRequestBody(testVMName, "base-image", 5),
				expectedErr: &HTTPError{
					Endpoint:   getVMEndpointRelURL(endpointCreate),
					StatusCode: http.StatusForbidden,
					Message:    htmlErrBody,
				},
			},
			method: http.MethodPost,
			testFn: func(t *testing.T, c *client) error {
				return c.CreateVM(context.Background(), testVMName, "base-image", 5)
			},
		},
		"with purge html response": {
			testCaseBase: testCaseBase{
				token:                   "token1",
				statusCode:              http.StatusForbidden,
				respBody:                []byte(htmlErrBody),
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointPurge),
				expectedRequestBody:     getExpectedPurgeResponseBody(testVMName),
				expectedErr: &HTTPError{
					Endpoint:   getVMEndpointRelURL(endpointPurge),
					StatusCode: http.StatusForbidden,
					Message:    htmlErrBody,
				},
			},
			method: http.MethodDelete,
			testFn: func(t *testing.T, c *client) error {
				return c.PurgeVM(context.Background(), testVMName)
			},
		},
		"with deploy html response": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusForbidden,
				respBody:                []byte(htmlErrBody),
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDeploy),
				expectedRequestBody:     getExpectedDeployRequestBody(testVMName),
				expectedErr: &HTTPError{
					Endpoint:   getVMEndpointRelURL(endpointDeploy),
					StatusCode: http.StatusForbidden,
					Message:    htmlErrBody,
				},
			},
			method: http.MethodPost,
			testFn: func(t *testing.T, c *client) error {
				_, err := c.DeployVM(context.Background(), testVMName)
				return err
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			runTestCase(t, tc.method, &tc.testCaseBase, func(c *client) error {
				return tc.testFn(t, c)
			})
		})
	}
}

func TestResponseTimeout(t *testing.T) {
	type timeoutTestCase struct {
		testCaseBase

		method string
		testFn testFuncWithContext
	}
	testCases := map[string]timeoutTestCase{
		"with status response taking too long and retry": {
			testCaseBase: testCaseBase{
				token:                   "token",
				backoffSettings:         testBackoff,
				statusCode:              http.StatusOK,
				expectedRequestAttempts: 0,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, testVMName),
				expectedErr:             backoff.ErrMaximumBackOffTimeElapsedExceeded,
			},
			method: http.MethodGet,
			testFn: func(ctx context.Context, t *testing.T, c *client) error {
				addr, err := c.GetSSHEndpoint(ctx, testVMName)
				assert.Equal(t, nil, addr, "expected empty address")
				return err
			},
		},
		"with create response taking too long and retry": {
			testCaseBase: testCaseBase{
				token:                   "token",
				backoffSettings:         testBackoff,
				statusCode:              http.StatusOK,
				expectedRequestAttempts: 0,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointCreate),
				expectedErr:             backoff.ErrMaximumBackOffTimeElapsedExceeded,
			},
			method: http.MethodPost,
			testFn: func(ctx context.Context, t *testing.T, c *client) error {
				return c.CreateVM(ctx, testVMName, "base-image", 4)
			},
		},
		"with purge response taking too long and retry": {
			testCaseBase: testCaseBase{
				token:                   "token",
				backoffSettings:         testBackoff,
				statusCode:              http.StatusOK,
				expectedRequestAttempts: 0,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointPurge),
				expectedErr:             backoff.ErrMaximumBackOffTimeElapsedExceeded,
			},
			method: http.MethodDelete,
			testFn: func(ctx context.Context, t *testing.T, c *client) error {
				return c.PurgeVM(ctx, testVMName)
			},
		},
		"with deploy response taking too long and retry": {
			testCaseBase: testCaseBase{
				token:                   "token",
				backoffSettings:         testBackoff,
				statusCode:              http.StatusOK,
				expectedRequestAttempts: 0,
				expectedRequestEndpoint: getVMEndpointRelURL("deploy"),
				expectedErr:             backoff.ErrMaximumBackOffTimeElapsedExceeded,
			},
			method: http.MethodPost,
			testFn: func(ctx context.Context, t *testing.T, c *client) error {
				_, err := c.DeployVM(ctx, testVMName)
				return err
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			runTestCase(t, tc.method, &tc.testCaseBase, func(c *client) error {
				testTimeout := time.Second
				c.client.Transport = &timeoutTransport{
					timeout:       testTimeout + 100*time.Millisecond,
					baseTransport: c.client.Transport,
				}

				ctx, cancel := context.WithTimeout(context.Background(), testTimeout)
				defer cancel()

				return tc.testFn(ctx, t, c)
			})
		})
	}
}

type timeoutTransport struct {
	timeout       time.Duration
	baseTransport http.RoundTripper
}

func (t *timeoutTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	time.Sleep(t.timeout)
	return t.baseTransport.RoundTrip(r)
}

func TestBrokenResponseFromOrkaAPI(t *testing.T) {
	type brokenResponseTestCase struct {
		testCaseBase

		method string
		testFn testFuncWithContext
	}
	testCases := map[string]brokenResponseTestCase{
		"with broken status response": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusOK,
				respBody:                nil,
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, testVMName),
				expectedRequestBody:     nil,
				expectedErr:             errors.New("cannot read body"),
			},
			method: http.MethodGet,
			testFn: func(ctx context.Context, t *testing.T, c *client) error {
				addr, err := c.GetSSHEndpoint(ctx, testVMName)
				assert.Equal(t, nil, addr, "expected empty address")
				return err
			},
		},
		"with broken create response": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusOK,
				respBody:                nil,
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointCreate),
				expectedRequestBody:     getExpectedCreateRequestBody(testVMName, "base-image", 3),
				expectedErr:             errors.New("cannot read body"),
			},
			method: http.MethodPost,
			testFn: func(ctx context.Context, t *testing.T, c *client) error {
				return c.CreateVM(ctx, testVMName, "base-image", 3)
			},
		},
		"with broken deploy response": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusOK,
				respBody:                nil,
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDeploy),
				expectedRequestBody:     getExpectedDeployRequestBody(testVMName),
				expectedErr:             errors.New("cannot read body"),
			},
			method: http.MethodPost,
			testFn: func(ctx context.Context, t *testing.T, c *client) error {
				_, err := c.DeployVM(ctx, testVMName)
				return err
			},
		},
		"with broken purge response": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusOK,
				respBody:                nil,
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointPurge),
				expectedRequestBody:     getExpectedPurgeResponseBody(testVMName),
				expectedErr:             errors.New("cannot read body"),
			},
			method: http.MethodDelete,
			testFn: func(ctx context.Context, t *testing.T, c *client) error {
				return c.PurgeVM(ctx, testVMName)
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			runTestCase(t, tc.method, &tc.testCaseBase, func(c *client) error {
				c.client.Transport = &brokenBodyTransport{baseTransport: c.client.Transport, err: tc.expectedErr}
				return tc.testFn(context.Background(), t, c)
			})
		})
	}
}

type brokenBodyTransport struct {
	baseTransport http.RoundTripper
	err           error
}

type brokenReadCloser struct {
	err error
}

func (b *brokenReadCloser) Read([]byte) (int, error) {
	return 0, b.err
}

func (b *brokenReadCloser) Close() error {
	return nil
}

func (t *brokenBodyTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	res, err := t.baseTransport.RoundTrip(r)
	if err != nil {
		return nil, err
	}

	res.Body = &brokenReadCloser{err: t.err}
	return res, nil
}

func TestMalformedResponseBody(t *testing.T) {
	type malformedResponseTestCase struct {
		testCaseBase

		testFn testFunc
	}
	testCases := map[string]malformedResponseTestCase{
		"with status returning malformed response body": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusOK,
				respBody:                []byte{0x1}, // Pass invalid JSON content
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, testVMName),
				expectedRequestBody:     nil,
				expectedErr:             &json.SyntaxError{},
			},
			testFn: func(t *testing.T, c *client) error {
				addr, err := c.GetSSHEndpoint(context.Background(), testVMName)
				assert.Equal(t, nil, addr, "expected empty address")
				return err
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				// Write some HTTP response status code, doesn't really matter which
				w.Header().Add("Content-Type", "application/json; charset=utf-8")
				w.WriteHeader(tc.statusCode)
				_, err := w.Write(tc.respBody.([]byte))
				assert.NoError(t, err)
			})
			ts := httptest.NewServer(h)
			defer ts.Close()

			baseURL, err := url.Parse(ts.URL)
			require.NoError(t, err)

			config := &Config{
				Logger:          logging.New(),
				BackoffSettings: tc.backoffSettings,
				Client:          ts.Client(),
			}

			c := New(baseURL, "token", config)

			// Act
			err = tc.testFn(t, c.(*client))

			// Assert
			assert.True(t, errors.As(err, &tc.expectedErr), "returned error %T is not of the expected type %T",
				err, tc.expectedErr)
		})
	}
}

func TestUnknownResponseContentType(t *testing.T) {
	const invalidContentType = "invalid content type"
	type malformedResponseTestCase struct {
		testCaseBase

		testFn testFunc
	}

	errMime := errors.New("mime: expected slash after first token")
	testCases := map[string]malformedResponseTestCase{
		"with status returning malformed content type": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusBadRequest,
				respBody:                []byte{}, // Not significant
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointStatus, testVMName),
				expectedRequestBody:     nil,
				expectedErr: fmt.Errorf(
					"parsing %s Content-Type %q: %w",
					getVMEndpointRelURL(endpointStatus, testVMName),
					invalidContentType,
					errMime,
				),
			},
			testFn: func(t *testing.T, c *client) error {
				addr, err := c.GetSSHEndpoint(context.Background(), testVMName)
				assert.Equal(t, nil, addr, "expected empty address")
				return err
			},
		},
		"with create returning malformed content type": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusBadRequest,
				respBody:                []byte{}, // Not significant
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointCreate),
				expectedRequestBody:     getExpectedCreateRequestBody("foo", "base-image", 4),
				expectedErr: fmt.Errorf(
					"parsing %s Content-Type %q: %w",
					getVMEndpointRelURL(endpointCreate),
					invalidContentType,
					errMime,
				),
			},
			testFn: func(t *testing.T, c *client) error {
				return c.CreateVM(context.Background(), "foo", "base-image", 4)
			},
		},
		"with purge returning malformed content type": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusBadRequest,
				respBody:                []byte{}, // Not significant
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointPurge),
				expectedRequestBody:     getExpectedPurgeResponseBody(testVMName),
				expectedErr: fmt.Errorf(
					"parsing %s Content-Type %q: %w",
					getVMEndpointRelURL(endpointPurge),
					invalidContentType,
					errMime,
				),
			},
			testFn: func(t *testing.T, c *client) error {
				return c.PurgeVM(context.Background(), testVMName)
			},
		},
		"with deploy returning malformed content type": {
			testCaseBase: testCaseBase{
				token:                   "token",
				statusCode:              http.StatusBadRequest,
				respBody:                []byte{}, // Not significant
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDeploy),
				expectedRequestBody:     getExpectedDeployRequestBody(testVMName),
				expectedErr: fmt.Errorf(
					"parsing %s Content-Type %q: %w",
					getVMEndpointRelURL(endpointDeploy),
					invalidContentType,
					errMime,
				),
			},
			testFn: func(t *testing.T, c *client) error {
				_, err := c.DeployVM(context.Background(), testVMName)
				return err
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Content-Type", invalidContentType)
				w.WriteHeader(tc.statusCode)
				_, err := w.Write(tc.respBody.([]byte))
				assert.NoError(t, err)
			})
			ts := httptest.NewServer(h)
			defer ts.Close()

			baseURL, err := url.Parse(ts.URL)
			require.NoError(t, err)

			config := &Config{
				Logger:          logging.New(),
				BackoffSettings: tc.backoffSettings,
				Client:          ts.Client(),
			}

			c := New(baseURL, "token", config)

			// Act
			err = tc.testFn(t, c.(*client))

			// Assert
			assert.EqualError(t, err, tc.expectedErr.Error())
		})
	}
}

func TestSendHTTPRequestWithRetries(t *testing.T) {
	type testCase struct {
		ctx         context.Context
		method      string
		reqEndpoint string
		reqBody     interface{}
		handler     func(t *testing.T, w http.ResponseWriter, r *http.Request)
		respBody    interface{}
		assertFn    func(t *testing.T, tc *testCase, err error)
	}
	testCases := map[string]testCase{
		"with invalid request body": {
			ctx:         context.Background(),
			method:      http.MethodGet,
			reqEndpoint: getVMEndpointRelURL(endpointStatus, testVMName),
			reqBody:     func() {}, // Specify an invalid request body
			handler: func(t *testing.T, w http.ResponseWriter, r *http.Request) {
				assert.Fail(t, "Should not be called")
			},
			respBody: nil, // Doesn't really matter for this test case
			assertFn: func(t *testing.T, tc *testCase, err error) {
				var expectedErr *json.UnsupportedTypeError
				assert.True(t, errors.As(err, &expectedErr), "returned error %T does not match %T", err, expectedErr)
			},
		},
		"with invalid context": {
			ctx:         nil,
			method:      http.MethodPost,
			reqEndpoint: getVMEndpointRelURL(endpointDeploy),
			reqBody:     getExpectedDeployRequestBody(testVMName),
			handler: func(t *testing.T, w http.ResponseWriter, r *http.Request) {
				assert.Fail(t, "Should not be called")
			},
			respBody: deployVMReqBody{},
			assertFn: func(t *testing.T, tc *testCase, err error) {
				assert.Error(t, err, "net/http: nil Context")
			},
		},
		"with invalid body on success response": {
			ctx:         context.Background(),
			method:      http.MethodPost,
			reqEndpoint: getVMEndpointRelURL(endpointDeploy),
			reqBody:     getExpectedDeployRequestBody(testVMName),
			handler: func(t *testing.T, w http.ResponseWriter, r *http.Request) {
				respBody := []byte{0x01}
				// Write HTTP response status code
				w.WriteHeader(http.StatusOK)

				_, err := w.Write(respBody)
				require.NoError(t, err)
			},
			respBody: []byte{0x01},
			assertFn: func(t *testing.T, tc *testCase, err error) {
				var expectedErr *json.SyntaxError
				assert.True(t, errors.As(err, &expectedErr), "returned error %T does not match %T", err, expectedErr)
			},
		},
		"with invalid body on error response": {
			ctx:         context.Background(),
			method:      http.MethodPost,
			reqEndpoint: getVMEndpointRelURL(endpointDeploy),
			reqBody:     getExpectedDeployRequestBody(testVMName),
			handler: func(t *testing.T, w http.ResponseWriter, r *http.Request) {
				respBody := []byte{0x01}
				w.Header().Add("Content-Type", "application/json; charset=utf-8")

				// Write HTTP response status code
				w.WriteHeader(http.StatusBadRequest)

				_, err := w.Write(respBody)
				require.NoError(t, err)
			},
			respBody: []byte{0x01},
			assertFn: func(t *testing.T, tc *testCase, err error) {
				var expectedErr *json.SyntaxError
				assert.True(
					t,
					errors.As(err, &expectedErr),
					"returned error %T does not match %T",
					err,
					expectedErr,
				)
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			// Arrange
			h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				tc.handler(t, w, r)
			})
			ts := httptest.NewServer(h)
			defer ts.Close()

			baseURL, err := url.Parse(ts.URL)
			require.NoError(t, err)

			config := &Config{
				Logger:          logging.New(),
				BackoffSettings: testBackoff,
				Client:          ts.Client(),
			}

			c := New(baseURL, "token", config)

			// Act
			err = c.(*client).sendHTTPRequestWithRetries(tc.ctx, tc.method, tc.reqEndpoint, tc.reqBody, &tc.respBody)

			// Assert
			tc.assertFn(t, &tc, err)
		})
	}
}

func serializeToJSON(t *testing.T, val interface{}) string {
	b, err := json.Marshal(val)
	require.NoError(t, err)
	return string(b)
}
