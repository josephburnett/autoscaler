package orka

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeployVM(t *testing.T) {
	respBody200 := deployVMResBody{
		IP:      "127.0.0.1",
		SSHPort: "22",
	}

	type deployVMTestCase struct {
		testCaseBase

		name string
	}

	testCases := map[string]deployVMTestCase{
		"deploy vm is successful": {
			testCaseBase: testCaseBase{
				token:                   "token1",
				statusCode:              http.StatusCreated,
				respBody:                respBody200,
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDeploy),
				expectedRequestBody:     getExpectedDeployRequestBody(testVMName),
				expectedErr:             nil,
			},
			name: testVMName,
		},
		"with invalid VM name": {
			testCaseBase: testCaseBase{
				token:      "token3",
				statusCode: http.StatusBadRequest,
				respBody: respBaseBody{
					Errors: respBodyErrors{
						{
							Message: "orka_vm_name should NOT be shorter than 1 characters",
						},
					},
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDeploy),
				expectedRequestBody:     getExpectedDeployRequestBody(""),
				expectedErr: &HTTPError{
					Endpoint:   getVMEndpointRelURL(endpointDeploy),
					StatusCode: http.StatusBadRequest,
					Message:    "",
					Errors:     []string{"orka_vm_name should NOT be shorter than 1 characters"},
				},
			},
			name: "",
		},
		"with invalid response IP": {
			testCaseBase: testCaseBase{
				token:      "token3",
				statusCode: http.StatusCreated,
				respBody: deployVMResBody{
					IP:      "foo",
					SSHPort: "22",
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDeploy),
				expectedRequestBody:     getExpectedDeployRequestBody(""),
				expectedErr:             NewInvalidResponseError("invalid IP format in \"foo\"", nil),
			},
			name: "",
		},
		"with invalid response port": {
			testCaseBase: testCaseBase{
				token:      "token3",
				statusCode: http.StatusCreated,
				respBody: deployVMResBody{
					IP:      "127.0.0.1",
					SSHPort: "foo",
				},
				expectedRequestAttempts: 1,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDeploy),
				expectedRequestBody:     getExpectedDeployRequestBody(""),
				expectedErr:             NewInvalidResponseError("parse SSH port \"foo\": strconv.Atoi: parsing \"foo\": invalid syntax", nil),
			},
			name: "",
		},
		"deploy vm is successful after retry on StatusServiceUnavailable": {
			testCaseBase: testCaseBase{
				token:                   "token1",
				statusCode:              http.StatusCreated,
				respBody:                respBody200,
				backoffSettings:         testBackoff,
				expectedRequestAttempts: 2,
				expectedRequestEndpoint: getVMEndpointRelURL(endpointDeploy),
				expectedRequestBody:     getExpectedDeployRequestBody(testVMName),
				expectedErr:             nil,
			},
			name: testVMName,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			runTestCase(t, http.MethodPost, &tc.testCaseBase,
				func(c *client) error {
					vm, err := c.DeployVM(context.Background(), tc.name)
					if err != nil {
						return err
					}

					assert.Equal(t, "127.0.0.1:22", vm.Addr.String())
					assert.Equal(t, "tcp", vm.Addr.Network())

					return nil
				})
		})
	}
}

func getExpectedDeployRequestBody(vmName string) deployVMReqBody {
	return deployVMReqBody{VMName: vmName}
}

func TestDeployVMReqBodyJSONSerialization(t *testing.T) {
	testCases := []deployVMReqBody{
		{
			VMName: "myorkavm1",
		},
		{
			VMName: "myorkavm2",
		},
	}

	for _, tc := range testCases {
		json := serializeToJSON(t, tc)
		expectedJSON := fmt.Sprintf(`{"orka_vm_name":"%s"}`, tc.VMName)

		assert.Equal(t, json, expectedJSON)
	}
}
