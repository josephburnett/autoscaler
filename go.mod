module gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be // indirect
	github.com/bmatcuk/doublestar v1.3.4
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/gliderlabs/ssh v0.3.2
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/jstemmer/go-junit-report v0.9.1
	github.com/masterzen/winrm v0.0.0-20190308153735-1d17eaf15943
	github.com/mitchellh/gox v1.0.1
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/afero v1.2.2
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.6.2-0.20200720104044-95a9d909e987
	github.com/urfave/cli v1.21.0
	gitlab.com/ayufan/golang-cli-helpers v0.0.0-20171103152739-a7cf72d604cd
	gitlab.com/gitlab-org/gitlab-runner v12.4.2-0.20191106123436-fc03aa348bac+incompatible
	go.opencensus.io v0.20.2 // indirect
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
	google.golang.org/api v0.3.1
	google.golang.org/grpc v1.19.1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
