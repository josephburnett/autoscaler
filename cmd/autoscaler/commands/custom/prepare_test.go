package custom

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	globalConfig "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/cli"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging/test"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/runner"
	mockExecutors "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/mocks/executors"
	mockProviders "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/mocks/providers"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers"
)

func prepareContext(t *testing.T) *cli.Context {
	err := os.Setenv("BUILD_FAILURE_EXIT_CODE", "1")
	require.NoError(t, err)
	err = os.Setenv("SYSTEM_FAILURE_EXIT_CODE", "1")
	require.NoError(t, err)
	defer func() {
		err = os.Unsetenv("BUILD_FAILURE_EXIT_CODE")
		require.NoError(t, err)
		err = os.Unsetenv("SYSTEM_FAILURE_EXIT_CODE")
		require.NoError(t, err)
	}()

	err = runner.InitAdapter()
	require.NoError(t, err)

	cfg := globalConfig.Global{
		Provider: t.Name(),
		OS:       t.Name(),
	}
	ctx := &cli.Context{}
	ctx.SetLogger(test.NewNullLogger())
	ctx.SetConfig(cfg)

	return ctx
}

func prepareProvider(t *testing.T) *mockProviders.Provider {
	mockProvider := &mockProviders.Provider{}
	providers.MustRegister(t.Name(), func(cfg globalConfig.Global, logger logging.Logger) (providers.Provider, error) {
		return mockProvider, nil
	})
	return mockProvider
}

func prepareExecutor(t *testing.T) *mockExecutors.Executor {
	mockExecutor := &mockExecutors.Executor{}
	executors.MustRegister(t.Name(), t.Name(), func(cfg globalConfig.Global, logger logging.Logger) (executors.Executor, error) {
		return mockExecutor, nil
	})
	return mockExecutor
}

func TestPrepare(t *testing.T) {
	t.Run("normal case", func(t *testing.T) {
		ctx := prepareContext(t)
		mockProvider := prepareProvider(t)
		defer mockProvider.AssertExpectations(t)
		mockExecutor := prepareExecutor(t)
		defer mockExecutor.AssertExpectations(t)

		mockProvider.On("VMName", mock.Anything).Return("vm-123")
		mockProvider.On("Create", nil, mock.Anything, mock.Anything).Return(nil)

		cmd := &PrepareCommand{}
		err := cmd.CustomExecute(ctx)
		assert.NoError(t, err)
	})

	t.Run("creation fails case", func(t *testing.T) {
		ctx := prepareContext(t)
		mockProvider := prepareProvider(t)
		defer mockProvider.AssertExpectations(t)
		mockExecutor := prepareExecutor(t)
		defer mockExecutor.AssertExpectations(t)

		mockProvider.On("VMName", mock.Anything).Return("vm-123")
		mockProvider.On("Create", nil, mock.Anything, mock.Anything).Return(assert.AnError)
		mockProvider.On("Delete", nil, mock.Anything).Return(nil)

		cmd := &PrepareCommand{}
		err := cmd.CustomExecute(ctx)
		assert.Error(t, err)
		assert.ErrorIs(t, err, assert.AnError)
	})

	t.Run("creation and delete fail case", func(t *testing.T) {
		ctx := prepareContext(t)
		mockProvider := prepareProvider(t)
		defer mockProvider.AssertExpectations(t)
		mockExecutor := prepareExecutor(t)
		defer mockExecutor.AssertExpectations(t)

		mockProvider.On("VMName", mock.Anything).Return("vm-123")
		mockProvider.On("Create", nil, mock.Anything, mock.Anything).Return(assert.AnError)
		mockProvider.On("Delete", nil, mock.Anything).Return(assert.AnError)

		cmd := &PrepareCommand{}
		err := cmd.CustomExecute(ctx)
		assert.Error(t, err)
		assert.Contains(t, err.Error(), "create and cleanup failed")
	})
}
